﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Library.Controllers
{
    public class GuestController : Controller
    {
        // GET: Guest
        public ActionResult GuestPage()
        {
            return View(HttpContext.Application["reviews"]);
        }

        [HttpPost]
        public ActionResult AddReview(string name, string date, string text)
        {
            (HttpContext.Application["reviews"] as List<(string, string, string)>)
                .Add((name, date, text));
            return new RedirectResult("/Guest/GuestPage");
        }
    }
}